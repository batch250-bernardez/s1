package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        int age;
        char middleName;

        // Variable Declaration vs. Initialization
        int x;
        int y = 0; // Initialization with declaration of variable

        // Initialization after declaration
        x = 1;

        // Output to the system
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data Types
        // Predefined within the Java programming language which is used for single-valued with limited capabilities

        // int - whole number values
        int wholeNumber = 100; // Maximum of 10 digits in int
        System.out.println(wholeNumber);

        // Long datatype
        // "L" is added at the end of the long number to be recognized.
        long worldPopulation = 78628811457878L;
        System.out.println(worldPopulation);

        // Float
        // Add "f" at the end of the float to be recognized
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        // "double" is floating point values
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        // "char" - can store single character only
        // Uses single quotes
        char letter = 'a';
        System.out.println(letter);

        // Boolean - true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        // Constants - all capslock
        // Java uses the "final" keyword so the variable's value cannot be changed.
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        // PRINCIPAL = 4000; this will result to an error


        // Non-primitive Data

        // String
        // Stores a sequence or array of characters
        // Strings are actually an object that can use methods
        String userName = "Jsmith";
        System.out.println(userName);

        // Sample string method
        int stringLength = userName.length();
        System.out.println(stringLength);
    }
}
